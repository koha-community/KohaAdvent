An Advent Calendar Site for Koha
================================

A clone of Beautiful Jekyll from GitHub using GitLab Pages to publish a Koha Advent Calendar

Getting started
---------------

Clone this repository, create a branch for the day you wish to write an article for.

- Create a new file in `_posts/` called `2020-12-01-intro.md`
  Edit it, and add:

~~~
  ---
  title: "home"
  author: tomas
  ---

  # Example headline!
  and so on..
~~~

- Create a second post called `2020-12-02-art.md` with an divider image this time:

~~~
  ---
  title: "Art"
  author: martin
  ---

  #### A new section- oh the humanity!
~~~


Nifty, right!

## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][(https://jekyllrb.com/docs/installation/) Jekyll - `sudo gem install github-pages`
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Visit [localhost:4000](http://localhost:4000) to see a live locally served preview.
1. Push changes and see them live!

The above commands should be executed from the root directory of this project.

