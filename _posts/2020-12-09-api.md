---
title: "Adding API routes"
cover-img: /assets/img/pexels-pixabay-56876.jpg
thumbnail-img: /assets/img/pexels-any-lane-5727949.jpg
share-img: /assets/img/pexels-any-lane-5727949.jpg
author: tomas
---

Today's post is about a feature that was added on *Koha 18.11*: the ability to add API routes using plugins.

In earlier posts we added a table, used DBI to perform CRUD operations on the controller script. Later we added
a cool Koha::Object(s) based class to abstract those table operations.

This post will try to highlight how easy is to add routes on top of Koha::Object-based classes.

## API plugin

In order for a plugin to be considered for adding API routes, it needs to implement two methods:
* **api_namespace**
* **api_routes**

The plugin system will completely skip plugins that don't implement both.

### **api_namespace**

API routes look pretty much like `/api/v1/path/to/a/route`. It is easy to notice that plugins have lots of flexibility
to add routes, so we need a way to avoid _route collissions_ or overwrites.

Plugin routes are always added inside a separate `/contrib` path, so they can never overwrite those from the core distribution.
Then, the **api_namespace** comes into play to avoid plugins overwriting each other's routes. In our example the method looks like:

```perl
sub api_namespace {
    my ($self) = $_;

    return 'fancy';
}
```

In this case, the resulting base path for the routes will be `/api/v1/contrib/fancy`.

There can still be collissions (i.e. two plugins using the same namespace), Koha will keep the first plugin implementing a namespace, and skip the rest of them.

### **api_routes**

Koha implements a RESTful API using the [OpenAPI v2](https://swagger.io/specification/v2/) specification language.
The spec is written in [JSON](https://www.json.org/json-en.html). You will need some time to get used to the structure
if you are not familiar already.

When it comes to plugins, they only introduce new _paths_, so you can focus on that part of the standard. I always find
the [OpenAPI map](https://openapi-map.apihandyman.io/) to be the best reference when writing API routes.

In order to tell Koha the _paths_ the plugin is adding, you need to implement a method that returns that said paths definition:

```perl
sub api_routes {
    my ($self) = $_;

    return <<SPEC;
{
  "/words": {
    "get": {
      "x-mojo-to": "FancyPlugin::Controller#list",
      "operationId": "listFancyWords",
      "tags": [
        "fun"
      ],
    ...
SPEC
}
```

When you get into writing API routes, you will notice this is not really handy:

* The spec can get lenghty, making the file unreadable.
* As this is a Perl file with inline JSON, editors won't work for syntax highlighting or showing errors.
* Same as above, but for linting the spec.

What we do is taking advantage of the `mbf_read` method plugins implement, and put the spec on a separate [bundle file](https://gitlab.com/koha-community/koha-advent/koha-plugin-fancyplugin/-/blob/master/Koha/Plugin/FancyPlugin/openapi.json):

```perl
use Mojo::JSON qw(decode_json);

sub api_routes {
    my ($self) = @_;

    my $spec_str = $self->mbf_read('openapi.json');
    my $spec     = decode_json($spec_str);

    return $spec;
}
```

## Controller class

If you look at the [openapi.json](https://gitlab.com/koha-community/koha-advent/koha-plugin-fancyplugin/-/blob/master/Koha/Plugin/FancyPlugin/openapi.json) file, you will notice an important entry on each verb, on each path: `x-mojo-to`. That attribute is used by Mojolicious to route the requests to the implementing methods:

```json
"/words": {
    "get": {
        "x-mojo-to": "FancyPlugin::Controller#list",
        "operationId": "listFancyWords",
        "tags": [
        "fun"
        ],
    ...
    }
}
```

The class name resolution step will append `Koha::Plugin::` to the class name, resulting in `Koha::Plugin::FancyPlugin::Controller`, and the _list()_ method being responsible of implementing the route.

Vendors usually add their names to the class name, or just name their classes longer, like in `Koha::Plugin::Com::Theke::INNReach::CircController`. In this case, for a _borrowerrenew_ controller method, the `x-mojo-to` will look like:

```json
"x-mojo-to": "Com::Theke::INNReach::CircController#borrowerrenew"
```

## Testing the API

Once we've got everything in place, it is time to check things are working. As a start, you need to restart Plack. You might also need to re-install the plugin:

```shell
$ kshell
$ perl misc/devel/install_plugins.pl
...
Installed Our fancy plugin version {VERSION}
All plugins successfully re-initialised
```

Uncaught exceptions on the API are usually reported to the `/var/log/koha/kohadev/api-error.log` file. And warnings and some other error reporting in `/var/log/koha/kohadev/plack-error.log`.

The sample API we implemented for the _FancyPlugin_ implements the following routes:

```shell
GET    /api/v1/contrib/fancy/words
GET    /api/v1/contrib/fancy/words/{fancy_word_id}
POST   /api/v1/contrib/fancy/words
PUT    /api/v1/contrib/fancy/words/{fancy_word_id}
DELETE /api/v1/contrib/fancy/words/{fancy_word_id}
```

You should be able to see them documented under _http://kohadev.myDNSname.org:8080/api/v1/.html_. If you don't find them, look at the logs while restarting Plack to see what's being reported.

For showing you how to try the API, I'll be using [Postman](https://www.postman.com/) which is a really cool tool, available for most operating systems. There are other tools, extensions for Firefox, etc. I found Postman to be the easiest to use, specially when dealing with OAuth2.

For the purpose of this tests, I have enabled the **RESTBasicAuth** system preference. And I use a regular _login_ an _password_ combination.

Let's try to retrieve the current fancy words:

![`GET /api/v1/contrib/fancy/words`](/KohaAdvent/assets/img/fancy_words/get_empty.png){: .mx-auto.d-block :}

an empty list is expected as we haven't added any fancy words yet. Let's add one:

![`POST /api/v1/contrib/fancy/words`](/KohaAdvent/assets/img/fancy_words/add.png){: .mx-auto.d-block :}

Notice a *fancy_word_id* and _timestamp_ have been assigned and returned. Let's try using the *fancy_word_id* to retrieve a single fancy word:

![`GET /api/v1/contrib/fancy/words/{fancy_word_id}`](/KohaAdvent/assets/img/fancy_words/get_single.png){: .mx-auto.d-block :}

Now add some more words, and repeat the first action to fetch them all:

![`GET /api/v1/contrib/fancy/words`](/KohaAdvent/assets/img/fancy_words/get_list.png){: .mx-auto.d-block :}

Let's say we want to update one of the words because we want another one:

![`PUT /api/v1/contrib/fancy/words/{fancy_word_id}`](/KohaAdvent/assets/img/fancy_words/update.png){: .mx-auto.d-block :}

And now delete a word:

![`DELETE /api/v1/contrib/fancy/words/{fancy_word_id}`](/KohaAdvent/assets/img/fancy_words/delete.png){: .mx-auto.d-block :}

If we try to retrieve the deleted word, it should tell us the resource doesn't exist:

![`GET /api/v1/contrib/fancy/words/{fancy_word_id}`](/KohaAdvent/assets/img/fancy_words/get_absent.png){: .mx-auto.d-block :}

And that's it! We've got full CRUD operations on fancy words. Later we will show how to use the API from the UI, replacing old-style controllers, for API-driven interactions!
