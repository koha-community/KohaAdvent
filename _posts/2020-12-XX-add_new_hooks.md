---
title: "WIP - Adding new hooks"
cover-img: /assets/img/path.jpg
thumbnail-img: /assets/img/thumb.png
share-img: /assets/img/path.jpg
author: joubu
---

In a previous post (TODO ref needed) we learnt how to use an existing hook.
If you need to write a behaviour that needs an additional hook, you will have
to implement it!

## Questions to ask yourself first

Before implementing  a new hook you should ask yourself a few questions:

* Do I really need a hook?

Maybe you actually don't need a hook. Sometimes a bit of javascript or CSS can
perfectly answer your needs, with minimum effort.

* Does it exist yet?

There is a list of available [Koha plugin hooks](https://wiki.koha-community.org/wiki/Koha_Plugin_Hooks), check if there is not one there yet that fits your needs!
Also better to double-check if someone else has not written it and is waiting for inclusion. Have a look at the [Koha bug tracker](https://bugs.koha-community.org/bugzilla3/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=REOPENED&bug_status=ASSIGNED&bug_status=In%20Discussion&bug_status=Needs%20Signoff&bug_status=Signed%20Off&bug_status=Passed%20QA&bug_status=Failed%20QA&bug_status=Patch%20doesn%27t%20apply&component=Plugin%20architecture&list_id=354377&query_format=advanced).

* Is it something everyone else will need?

It happens that you need something, but actually lot of people wants it as well.
For instance if you need a new action log when a given object is modified and it is not implemented yet into Koha, you will prefer to write a core feature and submit it to make it available to the other users.

If in doubt, ask the community.

## Things to take into account before starting

The constraint will be that you will have to wait for the hook to be available in a stable version (usually new hooks won't be backported).
Your plugin will have to specify this version as the minimum version it supports.

As your plugin adoption will depend on the Koha community acceptance, you should make sure the hook will be integrated into master before starting developping the plugin.

## Implementing a new hook

First thing first, where are you going to place the hook? This is the more important question. Put it at the best place, where you will have the hand on what you are trying to achieve.

Maybe you will need to have two hooks, one `pre` and one `post` action. For instance if you want to trigger something when an authorised value is modified, you may want to either modify the Koha::AuthorisedValue object before it is updated into the database, or trigger an action once it has been correctly inserted.

Lets say you want to force the value of `authorised_values.imageurl`, whenever the authorised value is modified.
A good place would be Koha::AuthorisedValue->store:

```perl
sub store {
    my ( $self ) = @_;
    if ( $self->in_storage ) { # The record exists already, we are modifying it
        $self = Koha::Plugins->call('before_av_modification', authorised_value => $self);
    }
    return self->SUPER::store;
}
```

However we already have similar hooks for biblios and items: `after_item_action` and `after_biblio_action`, to deal with creation and modification.
To stick with this pattern we are going to improve our hook to support the creation as well

```perl
sub store {
    my ($self) = @_;
    my $plugin_action = 'create';
    if ( $self->in_storage ) {  # The record exists already, we are modifying it
        $plugin_action = 'modify';
    }

    $self = $self->_before_av_action_hooks(
        { action => $plugin_action }
    );

    return self->SUPER::store;
}

sub _before_av_action_hooks {
    my ( $self, $params ) = @_;

    Koha::Plugins->call(
        'before_av_action',
        {
            action           => $params->{action},
            authorised_value => $self,
        }
    );
}
```

and our plugin can now implement this new hook:

```perl
sub before_av_modification {
    my ( $self, $params ) = @_;

    my action = $params->{action};
    my $av    = $params->{authorised_value};

    if ( $action eq 'modify' ) {
        $av->imageurl(
            sprintf( "%s/%s.png",
                $self->retrieve_date('image_base_url'), $av->lib )
        );
    } # else we don't want to do nothing

    return $av;
}
```

On the other hand, if we would have needed to build a new image for this authorised value, a post modification hook would have been needed. This way we make sure we will not have generated an image if the modification in the DB failed.

```perl
sub store {
    my ($self) = @_;
    my $plugin_action = 'create';
    if ( $self->in_storage ) {  # The record exists already, we are modifying it
        $plugin_action = 'modify';
    }
    $self = $self->_before_av_action_hooks(
        { action => $plugin_action }
    );

    my $result = $self->SUPER::store;

    $self->get_from_storage->_after_av_action_hooks({ action => $plugin_action });

    return self->SUPER::store;
}
```

The `after_av_action_hooks` method and `post` actions plugin's hooks are left as an exercise for the reader.

Now you know how to create a new hook and if it is a good idea to implement such hooks. Your plugins don't have limits anymore!
