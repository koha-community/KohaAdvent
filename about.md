---
title: "About"
author: martin
---

# *Whoa, an advent calendar for Koha!*

**I've been wanting to do a KohaILS advent calendar for years...**

…inspired by the likes of the [perl advent calendars](http://www.perladvent.org/),
I wanted to do something similar for Koha.

So, I went and badgered the right people, and here we are!

If you have something cool to show off, then get in touch.. and most of all, enjoy.

{% include cite.html %}
